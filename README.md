**This is the repository for some of our work with the D2L Brightspace API**

We have decided to share some of the tools and other helpful information that we have developed and use to help others learn and be able to use D2L's Valience API.


If you have any questions, concerns, or would like to contribute:  Please contact me at jbarger@georgiasouthern.edu


**PS - Please use any of the code here at your own risk** 
---


